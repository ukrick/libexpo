desc 'Generate slugs for existing models'

require 'active_record'

namespace :libexpo do
  task :generate_slugs => :environment do

    Node.find_each(&:save)
    Category.find_each(&:save)
    Collection.find_each(&:save)
    Event.find_each(&:save)
    Article.find_each(&:save)
    Book.find_each(&:save)
    Publication.find_each(&:save)

  end
end
