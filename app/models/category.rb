class Category < ActiveRecord::Base

  has_many :collections, :inverse_of => :category
  has_many :publications, :inverse_of => :category

  attr_accessible :name, :description, :is_public
  attr_accessible :collection_ids, :publication_ids

  validates :name, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  extend FriendlyId
  friendly_id :name

  rails_admin do
    configure :description, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    configure :slug do
      hide
    end
    list do
      field :is_public
      field :name
      field :updated_at
      items_per_page 10
    end
    show do
      field :is_public
      field :name
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :updated_at
    end
  end
end
