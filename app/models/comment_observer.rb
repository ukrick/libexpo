class CommentObserver < ActiveRecord::Observer
  def after_create(comment)
    JabberNotifier.comment(comment.username, comment.description)
  end
end
