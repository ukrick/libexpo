class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :collection, :inverse_of => :comments
  belongs_to :article, :inverse_of => :comments
  belongs_to :book, :inverse_of => :comments

  attr_accessible :user_id, :collection_id, :article_id, :book_id, :is_public, :username, :description

  validates :user, :presence => true
  validates :username, :presence => {:message => I18n.t('error_validates_username')}
  validates :description, :presence => {:message => I18n.t('error_validates_description')}

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :collection do
      inverse_of :comments
    end
    configure :article do
      inverse_of :comments
    end
    configure :book do
      inverse_of :comments
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    configure :username do
      hide
    end
    list do
      field :user
      field :collection
      field :article
      field :book
      field :is_public
      field :updated_at
      items_per_page 10
    end
    show do
      field :user
      field :collection
      field :article
      field :book
      field :is_public
      field :description
      field :updated_at
    end
  end
end
