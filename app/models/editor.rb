class Editor

  TOOLBAR = [
      %w(Source - Save NewPage DocProps Preview Print - Templates),
      %w(Cut Copy Paste PasteText PasteFromWord - Undo Redo),
      %w(Find Replace - SelectAll),
      '/',
      %w(Bold Italic Underline Strike Subscript Superscript - RemoveFormat),
      %w(NumberedList BulletedList - Outdent Indent - Blockquote - JustifyLeft JustifyCenter JustifyRight JustifyBlock - BidiLtr BidiRtl),
      %w(Link Unlink Anchor),
      %w(richImage richFile MediaEmbed Flash Table HorizontalRule SpecialChar),
      '/',
      %w(Styles Format Font FontSize),
      %w(TextColor BGColor),
      %w(Maximize ShowBlocks)
  ]

  STYLESET = [
      # Block-level styles
      {name: I18n.t(:editor_styles_h2), element: 'h2', styles: {color: 'Blue'}},
      {name: I18n.t(:editor_styles_h3), element: 'h3', styles: {color: 'Red'}},
      # Inline styles
  ]
end
