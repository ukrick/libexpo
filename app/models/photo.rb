class Photo < ActiveRecord::Base
  belongs_to :user
  belongs_to :collection, :inverse_of => :photos

  attr_accessible :user_id, :collection_id, :image, :title, :description

  attr_accessible :tag_list
  acts_as_taggable

  validates :user, :collection, :image, :description, :presence => true
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :title do
      def render
        bindings[:view].render :partial => 'shared/title', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :collection
    configure :image, :rich_picker do
      config({
                 :allowed_styles => [:thumb]
             })
    end
    configure :tag_list do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_tags', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    list do
      field :user
      field :collection
      field :image do
        formatted_value do
          bindings[:view].tag(:img, {:src => bindings[:object].image})
        end
      end
      field :title
      field :updated_at
      items_per_page 10
    end
    show do
      field :user
      field :collection
      field :image do
        formatted_value do
          bindings[:view].tag(:img, {:src => bindings[:object].image})
        end
      end
      field :title
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :updated_at
    end
  end

  searchable do
    text :description
    time :updated_at
    #string for faceting
    string :search_class
  end

  def search_class
    self.class.name
  end
end
