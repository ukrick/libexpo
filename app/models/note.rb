class Note < ActiveRecord::Base
  belongs_to :user
  belongs_to :author
  belongs_to :collection
  belongs_to :article
  belongs_to :book
  belongs_to :publication, :inverse_of => :notes

  attr_accessible :title, :description, :is_public
  attr_accessible :user_id, :author_id, :publication_id, :collection_id, :article_id, :book_id

  validates :user, :presence => true
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :author do
      def render
        bindings[:view].render :partial => 'shared/author', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :publication
    configure :collection do
      hide
    end
    configure :article do
      hide
    end
    configure :book do
      inverse_of :note
    end
    configure :title do
      def render
        bindings[:view].render :partial => 'shared/title', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :description, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    list do
      field :publication
      field :book
      field :title
      field :is_public
      field :updated_at
      items_per_page 10
    end
    show do
      field :publication
      field :book
      field :title
      field :is_public
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :updated_at
    end
  end
end
