class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities

    return unless user # no permissions for guest

    alias_action :index, :show, :to => :read
    alias_action :new, :create, :to => :write
    alias_action :edit, :update, :to => :modify
    alias_action :destroy, :confirm_destroy, :to => :delete

    if user
      can :access, :rails_admin
      can :dashboard
      can :manage, Node
      can :manage, Category
      can :manage, Collection
      can :manage, Publication
      can :manage, Event
      can :manage, Article
      can :manage, Book
      can :manage, Photo
      can :manage, Comment
      can :manage, Note
      can :manage, Tag
      if user.customer?
        cannot :manage, Node
        cannot :manage, Category
        cannot :manage, Collection
        cannot :manage, Publication
        cannot :manage, Event
        cannot :manage, Article
        cannot :manage, Book
        cannot :manage, Photo
        cannot :manage, Comment
        cannot :manage, Note
        cannot :manage, Tag
        cannot :manage, User
        can [:show, :modify], User, :id => user.id, :role => I18n.t(:app_role_customer)
        can :read, Category
        can :read, Collection
        can :read, Article
        can :read, Book
        can :manage, Comment, :user_id => user.id
        can :manage, Publication, :author_id => user.id
        can :manage, Note, :author_id => user.id
      elsif user.operator?
        cannot :manage, User
        cannot :delete, Article, :id => 1
        can [:show, :modify], User, :id => user.id, :role => I18n.t(:app_role_operator)
        can :manage, User, :role => I18n.t(:app_role_customer)
      elsif user.admin?
        can :read, User
        cannot [:modify, :delete], User, :id => 1
        cannot :delete, Article, :id => 1
        can [:show, :modify], User, :id => user.id
      elsif user.super_admin?
        can :manage, :all
      end
    end
  end
end
