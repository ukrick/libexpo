class Role

  SUPER_ADMIN       = I18n.t(:app_role_super_administrator)
  ADMIN             = I18n.t(:app_role_administrator)
  OPERATOR          = I18n.t(:app_role_operator)
  CUSTOMER          = I18n.t(:app_role_customer)
  ANONYMOUS         = I18n.t(:app_role_anonymous)

  ROLES = [ SUPER_ADMIN, ADMIN, OPERATOR, CUSTOMER, ANONYMOUS ]

end
