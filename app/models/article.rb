class Article < ActiveRecord::Base
  belongs_to :user
  belongs_to :collection, :inverse_of => :articles
  belongs_to :node, :inverse_of => :articles

  has_many :comments, :inverse_of => :article, :dependent => :destroy

  attr_accessible :user_id, :node_id, :collection_id, :title, :image, :description, :is_public
  attr_accessible :comment_ids

  attr_accessible :tag_list
  acts_as_taggable

  extend FriendlyId
  friendly_id :title

  validates :user, :description, :presence => true
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :collection
    configure :comments do
      read_only true
    end
    configure :image, :rich_picker do
      config({
                 :allowed_styles => [:thumb]
             })
    end
    configure :title do
      def render
        bindings[:view].render :partial => 'shared/title', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :description, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :tag_list do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_tags', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    configure :slug do
      hide
    end
    list do
      field :user
      field :is_public
      field :collection
      field :title
      items_per_page 10
    end
    show do
      field :user
      field :is_public
      field :collection
      field :title
      field :image do
        formatted_value do
          bindings[:view].tag(:img, {:src => bindings[:object].image})
        end
      end
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :updated_at
    end
  end

  searchable do
    text :description
    time :updated_at
    #string for faceting
    string :search_class
  end

  def search_class
    self.class.name
  end
end
