class Publication < ActiveRecord::Base
  belongs_to :user
  belongs_to :author
  belongs_to :category, :inverse_of => :collections

  has_many :notes, :inverse_of => :publication, :dependent => :destroy

  attr_accessible :description, :is_public, :slug
  attr_accessible :user_id, :author_id, :category_id, :title, :image
  attr_accessible :note_ids

  validates :user, :presence => true
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  extend FriendlyId
  friendly_id :title

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :author do
      def render
        bindings[:view].render :partial => 'shared/author', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :category do
      inverse_of :publications
    end
    configure :notes do
      inverse_of :publication
    end
    configure :image, :rich_picker do
      config({
                 :allowed_styles => [:thumb]
             })
    end
    configure :title do
      def render
        bindings[:view].render :partial => 'shared/title', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :description, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    configure :slug do
      hide
    end
    list do
      field :author
      field :category
      field :title
      field :is_public
      field :notes do
        read_only true
      end
      field :updated_at
      items_per_page 10
    end
    show do
      field :author
      field :category
      field :title
      field :is_public
      field :notes do
        read_only true
      end
      field :image do
        formatted_value do
          bindings[:view].tag(:img, {:src => bindings[:object].image})
        end
      end
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :updated_at
    end
  end
end
