class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :username, :role
  attr_accessible :image, :first_name, :middle_name, :last_name, :mobile, :title

  has_many :collections
  has_many :articles
  has_many :comments
  has_many :publications
  has_many :notes

  validates :username, :role, :presence => true

  def role?(base_role)
    role.present? && Role::ROLES.index(base_role.to_s) <= Role::ROLES.index(role)
  end

  def super_admin?
    role?(Role::SUPER_ADMIN)
  end

  def admin?
    role?(Role::ADMIN)
  end

  def operator?
    role?(Role::OPERATOR)
  end

  def customer?
    role?(Role::CUSTOMER)
  end

  def anonymous?
    role?(Role::ANONYMOUS)
  end

  def self.current_user
    Thread.current[:current_user]
  end

  def self.current_user=(user)
    Thread.current[:current_user] = user
  end

  # First, middle and last name of the person
  #
  # @example first_name == 'John', last_name == 'Doe', middle_name == 'William'
  #    current_user.profile_description  => 'John W. Doe'
  # @param [ none ]
  # @return [ String ] first, middle and last name
  def profile_description
    desc = " #{last_name.capitalize if last_name.present?} #{first_name.truncate(3, :omission => '. ').capitalize if first_name.present?} #{middle_name.truncate(3, :omission => '. ').capitalize if middle_name.present?}"
    if desc.strip.empty?
      username
    else
      desc
    end
  end
end
