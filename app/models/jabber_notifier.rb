class JabberNotifier

  def self.comment(username, message)
    sender = Jabber::JID.new('manager@help.adidonntu.org.ua')
    client = Jabber::Client.new(sender)
    client.connect '192.168.1.90'
    client.auth 'adiqueen'
    client.send(Jabber::Presence.new.set_show(:chat).set_status('Message!'))
    recipients = [Jabber::JID.new('4z2@help.adidonntu.org.ua'), Jabber::JID.new('4z22@help.adidonntu.org.ua')]

    subject = I18n.t(:node_virtual_exhibitions) + '. ' + I18n.t('activerecord.attributes.comment.description') + '.'
    body = I18n.t(:app_role_anonymous) + ' ' + username + ': ' + message
    recipients.each do |recipient|
      thread = Jabber::Message::new(recipient, body).set_type(:normal).set_id('1').set_subject(subject)
      client.send(thread)
    end

    # Rich text messages for Jabber client like Pidgin or Kopete.
    #html = REXML::Element::new('html')
    #html.add_namespace('http://jabber.org/protocol/xhtml-im')
    #body = REXML::Element::new('body')
    #body.add_namespace('http://www.w3.org/1999/xhtml')
    #span = REXML::Element::new('span')
    #span.add_attribute('style', 'color:blue')
    #text = I18n.t(:app_role_customer) + ' ' + username + ': ' + message
    #span.add_text(text)
    #body.add(span)
    #html.add(body)
    #
    #thread = Jabber::Message::new(recipient)
    #thread.add_element(html)
    #thread.set_type(:normal).set_id('1').set_subject(subject)
    #client.send(thread)
    client.close
  end

end
