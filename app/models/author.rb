class Author < User
  has_many :publications
  has_many :notes
end
