class Collection < ActiveRecord::Base
  belongs_to :user
  belongs_to :category, :inverse_of => :collections

  has_many :articles, :inverse_of => :collection, :dependent => :destroy
  has_many :books, :inverse_of => :collection, :dependent => :destroy
  has_many :photos, :inverse_of => :collection, :dependent => :destroy
  has_many :comments, :inverse_of => :collection, :dependent => :destroy

  attr_accessible :user_id, :category_id, :title, :description, :afterword, :is_public
  attr_accessible :article_ids, :book_ids, :photo_ids, :comment_ids

  attr_accessible :tag_list
  acts_as_taggable

  extend FriendlyId
  friendly_id :title

  validates :user, :category, :presence => true
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 255}

  rails_admin do
    configure :user do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_user', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :category do
      inverse_of :collections
    end
    configure :title do
      def render
        bindings[:view].render :partial => 'shared/title', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :description, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :afterword, :rich_editor do
      config({
                 :toolbar => Editor::TOOLBAR,
                 :stylesSet => Editor::STYLESET,
                 :width => '99%', :height => '200px'
             })
    end
    configure :articles do
      inverse_of :collection
    end
    configure :books do
      inverse_of :collection
    end
    configure :photos do
      inverse_of :collection
    end
    configure :comments do
      read_only true
    end
    configure :tag_list do
      def render
        bindings[:view].render :partial => 'rails_admin/main/global_edit_tags', :locals => {:field => self, :form => bindings[:form]}
      end
    end
    configure :updated_at do
      date_format :long
      read_only true
    end
    configure :slug do
      hide
    end
    list do
      field :user
      field :is_public
      field :category
      field :title
      field :updated_at
      items_per_page 10
    end
    show do
      field :user
      field :is_public
      field :category
      field :title
      field :description do
        pretty_value do
          bindings[:object].description.html_safe
        end
      end
      field :afterword do
        pretty_value do
          bindings[:object].afterword.html_safe
        end
      end
      field :updated_at
    end
  end
end
