function showCloud() {
    if (!$('#main_canvas').tagcanvas({
        shape: 'vring(0.0)',
        stretchX: 3.8,
        lock: 'y',
        imageScale: null,
        outlineColour: 'transparent',
        outlineMethod: 'block',
        reverse: true,
        depth: 0.4,
        maxSpeed: 0.03
    }, 'carousel')) {
        $('#carousel_container').hide();
    }
    if (!$('#tag_canvas').tagcanvas({
        textColour: '#0000FF',
        outlineColour: 'transparent',
        outlineThickness: 0.2,
        reverse: true,
        depth: 0.8,
        maxSpeed: 0.05,
        zoom: 0.8
    }, 'tags')) {
        $('#tags_container').hide();
    }
}
