function showMessages() {
    $('.notice').hide();
    $('.alert').hide();
    $("#new_comment")
        .bind("ajax:success", function (event, data, status, xhr) {
            $(this).find('.alert').empty();
            $(this).find('input:text,textarea').val('');
            var success = $("#message").data('success');
            $('.notice').show();
            $('.alert').hide();
            $(this).find('.notice').html(success);
        })
        .bind("ajax:error", function (event, xhr, status, error) {
            var responseObject = $.parseJSON(xhr.responseText), errors = $('<ul/>');
            $.each(responseObject, function (index, value) {
                errors.append('<li>' + value + '</li>');
            });
            $('.notice').hide();
            $('.alert').show();
            $(this).find('.alert').html(errors);
        });
}
