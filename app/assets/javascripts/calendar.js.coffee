$(document).ready ->
  $('#fc').fullCalendar
    theme: 'smoothness',
    monthNames: ['Январь',
                 'Февраль',
                 'Март',
                 'Апрель',
                 'Май',
                 'Июнь',
                 'Июль',
                 'Август',
                 'Сентябрь',
                 'Октябрь',
                 'Ноябрь',
                 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    buttonText: {
      today: 'сегодня',
      month: 'месяц',
      week: 'неделя',
      day: 'день'
    },
    header: {
      left: 'prevYear,nextYear',
      center: 'title',
      right: 'month,basicWeek,basicDay today prev,next'
    },
    timeFormat: 'h:mm t{ - h:mm t} ',
    events: '/events.json',
    eventClick: (event) ->
      window.open('/events/' + event.id, '_self')
