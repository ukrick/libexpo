function showClock() {
    var date = new Date();
    var hours = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    var seconds = date.getSeconds().toString();
    var ctime;
    if (hours <= 9)
        hours = "0" + hours;
    if (minutes <= 9)
        minutes = "0" + minutes;
    if (seconds <= 9)
        seconds = "0" + seconds;
    ctime = hours + ":" + minutes + ":" + seconds;
    document.getElementById("clock").innerHTML = ctime;
    setTimeout(showClock, 1000);
}

