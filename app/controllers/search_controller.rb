class SearchController < ApplicationController

  def show
    @search = search(params[:query])
    @results = @search.results

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  protected
  def search(q)
    Sunspot.search [Event, Book, Article, Photo] do
      keywords q
      order_by :updated_at, :desc
      paginate(:page => params[:page], :per_page => 10)
    end
  end
end
