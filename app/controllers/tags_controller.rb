class TagsController < ApplicationController
  def index
    if params[:tag]
      @collections = Collection.tagged_with(params[:tag])
      @events = Event.tagged_with(params[:tag])
      @articles = Article.tagged_with(params[:tag])
      @books = Book.tagged_with(params[:tag])
    end
  end
end
