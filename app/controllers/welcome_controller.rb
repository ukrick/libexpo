class WelcomeController < ApplicationController

  skip_authorization_check

  def index
    @article = Article.find(1)
    collections = Collection.where('`is_public`')
    collections.present? ? @photos = collections.last.books.limit(9) : @photos = []
  end
end
