class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  check_authorization :unless => :devise_controller?

  before_filter :set_locale, :set_current_user, :set_default_values

  def set_current_user
    User.current_user = User.find_by_id(session[:user_id])
  end

  def set_default_values
    @nodes = Node.find_by_sql('SELECT `id`, `name` FROM `nodes` WHERE `is_public`')
    @categories = Category.find_by_sql('SELECT `id`, `name` FROM `categories` WHERE `is_public`')
    #@free_articles = Article.where('`is_public`').where(:collection_id => nil).where('`id` > 1').order('`created_at` DESC')
    @books_on_shelf = Book.order('created_at desc').limit 26
    @generator = ColorGenerator.new saturation: 0.3, lightness: 0.75
    if params[:id].present?
      @user = User.find(params[:id].to_i) if params[:model_name] == 'user'
      @collection = Collection.find(params[:id].to_i) if params[:model_name] == 'collection'
      @event = Event.find(params[:id].to_i) if params[:model_name] == 'event'
      @article = Article.find(params[:id].to_i) if params[:model_name] == 'article'
      @book = Book.find(params[:id].to_i) if params[:model_name] == 'book'
    end
    tags = Collection.tag_counts + Article.tag_counts + Book.tag_counts
    @tags = tags.uniq
    @branchs = @nodes.each_with_object([]) do |node, nodes|
      node.articles.where('`is_public`').each_with_object([]) do |article, articles|
        @articles = articles << {data: article.title, attr: {id: article.normalize_friendly_id(article.slug)}, metadata: {href: "/articles/#{article.normalize_friendly_id(article.slug)}"}}
      end
      nodes << {data: node.name, attr: {id: node.normalize_friendly_id(node.name)}, metadata: {href: "/nodes/#{node.normalize_friendly_id(node.name)}"}, children: node.articles.where('`is_public`').present? ? @articles : 0}
    end
    @collections = @categories.each_with_object([]) do |category, categories|
      category.collections.where('`is_public`').each_with_object([]) do |collection, collections|
        @collections = collections << {data: collection.title, attr: {id: collection.normalize_friendly_id(collection.slug)}, metadata: {href: "/collections/#{collection.normalize_friendly_id(collection.slug)}"}}
      end
      categories << {data: category.name, attr: {id: category.normalize_friendly_id(category.name)}, metadata: {href: "/categories/#{category.normalize_friendly_id(category.name)}"}, children: category.collections.where('`is_public`').present? ? @collections : 0}
    end
    @publications = @categories.each_with_object([]) do |category, categories|
      category.publications.where('`is_public`').each_with_object([]) do |publication, publications|
        @publications = publications << {data: publication.title, attr: {id: publication.normalize_friendly_id(publication.slug)}, metadata: {href: "/publications/#{publication.normalize_friendly_id(publication.slug)}"}}
      end
      categories << {data: category.name, attr: {id: category.normalize_friendly_id(category.name)}, metadata: {href: "/categories/#{category.normalize_friendly_id(category.name)}"}, children: category.publications.where('`is_public`').present? ? @publications : 0}
    end
    @categories.present? ? @exhibitions = [{data: t(:node_virtual_exhibitions), attr: {id: 'node0'}, metadata: {href: '/categories'}, children: @collections}, {data: t('activerecord.models.publication'), attr: {id: 'node1'}, metadata: {href: '/publications'}, children: @publications}, {data: t(:node_events), attr: {id: 'node2'}, metadata: {href: '/events'}, children: []}] : @exhibitions = []
  end

  # Setting the Locale from the Client Supplied Information.
  public
  def set_locale
    #logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
    #logger.debug "* Locale set to '#{I18n.locale}'"
    I18n.locale = accept_language_header
  end

  private
  def accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  # Custom Error Handling
  unless config.consider_all_requests_local
    rescue_from Exception, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
    rescue_from ActionController::RoutingError, :with => :render_not_found
    rescue_from ActionController::UnknownController, :with => :render_not_found
    # graceful error handling for cancan authorization exceptions
    rescue_from CanCan::AccessDenied, :with => :render_unauthorized
  end

  private
  def render_unauthorized(exception)
    respond_to do |format|
      format.html do
        if current_user
          flash[:error] = t(:error_authorization_failure)
          @page_trace = exception.message
          render :template => "/errors/unauthorized/index", :status => 401, :layout => 'error'
        else
          store_location
          redirect_to '/admin/sign_in' and return
        end
      end
      format.xml do
        request_http_basic_authentication 'Web Password'
      end
      format.json do
        render :text => "Not Authorized \n", :status => 401, :layout => 'error'
      end
    end
  end

  private
  def render_forbidden(exception)
    @page_title = t :error_forbidden_page_title
    @page_trace = exception.message
    render :template => "/errors/forbidden/index", :status => 403, :layout => 'error'
  end

  private
  def render_not_found(exception)
    @page_title = t :error_not_found_page_title
    @page_trace = exception.message
    render :template => "/errors/not_found/index", :status => 404, :layout => 'error'
  end

  private
  def render_unprocessable(exception)
    @page_title = t :error_unprocessable_entity_page_title
    @page_trace = exception.message
    render :template => "/errors/unprocessable_entity/index", :status => 422, :layout => 'error'
  end

  private
  def render_error(exception)
    @page_title = t :error_server_error_page_title
    @page_trace = exception.message
    #render :template => "/errors/server_error/index", :status => 500, :layout => 'error'
  end

  private
  def store_location
    # disallow return to login, logout, sign up pages
    disallowed_urls = %w(sign_up sign_in sign_out)
    disallowed_urls.map! { |url| url[/\/\w+$/] }
    unless disallowed_urls.include?(request.fullpath)
      session[:return_to] = request.fullpath.gsub('//', '/')
    end
  end
end

