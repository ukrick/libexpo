class PublicationsController < ApplicationController

  load_and_authorize_resource
  skip_authorize_resource :only => [:index, :show]

  # GET /publications
  # GET /publications.json
  def index
    @authors = Author.joins(:publications).where(role: I18n.t(:app_role_customer)).uniq
    @categories = Category.find_by_sql('SELECT `id`, `name` FROM `categories` WHERE `is_public`')
    @publications = @categories.each_with_object([]) do |category, categories|
      category.publications.where('`is_public`').each_with_object([]) do |publication, publications|
        @publications = publications << {data: publication.title, attr: {id: publication.normalize_friendly_id(publication.slug)}, metadata: {href: "/publications/#{publication.normalize_friendly_id(publication.slug)}"}}
      end
      categories << {data: category.name, attr: {id: category.normalize_friendly_id(category.name)}, metadata: {href: "/categories/#{category.normalize_friendly_id(category.name)}"}, children: category.publications.where('`is_public`').present? ? @publications : 0}
    end
    @publications_authors = @authors.each_with_object([]) do |author, authors|
      author.publications.where('`is_public`').each_with_object([]) do |publication, publications|
        @publications_authors = publications << {data: publication.title, attr: {id: publication.normalize_friendly_id(publication.slug)}, metadata: {href: "/publications/#{publication.normalize_friendly_id(publication.slug)}"}}
      end
      authors << {data: author.profile_description, attr: {id: author.username}, metadata: {href: "/authors/#{author.id}"}, children: author.publications.where('`is_public`').present? ? @publications_authors : 0}
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: [@publications, @publications_authors] }
    end
  end

  # GET /publications/1
  # GET /publications/1.json
  def show
    @publication = Publication.find(params[:id])
    @notes = @publication.notes.where('`collection_id` IS NOT NULL OR `article_id` IS NOT NULL OR `book_id` IS NOT NULL').where('`is_public`').order('`created_at` DESC')

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @publication }
    end
  end

  # GET /publications/new
  # GET /publications/new.json
  def new
    @publication = Publication.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @publication }
    end
  end

  # GET /publications/1/edit
  def edit
    @publication = Publication.find(params[:id])
  end

  # POST /publications
  # POST /publications.json
  def create
    @publication = Publication.new(params[:publication])

    respond_to do |format|
      if @publication.save
        format.html { redirect_to @publication, notice: 'Publication was successfully created.' }
        format.json { render json: @publication, status: :created, location: @publication }
      else
        format.html { render action: "new" }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /publications/1
  # PUT /publications/1.json
  def update
    @publication = Publication.find(params[:id])

    respond_to do |format|
      if @publication.update_attributes(params[:publication])
        format.html { redirect_to @publication, notice: 'Publication was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publications/1
  # DELETE /publications/1.json
  def destroy
    @publication = Publication.find(params[:id])
    @publication.destroy

    respond_to do |format|
      format.html { redirect_to publications_url }
      format.json { head :no_content }
    end
  end
end
