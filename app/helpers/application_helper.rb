module ApplicationHelper

  def app_meta_tag
    if protect_against_forgery?
      %(<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="author" content="#{t :meta_author}"/>
      <meta name="description" content="#{t :meta_description}"/>
      <meta name="keywords" content="#{t :meta_keywords}"/>).html_safe
    end
  end

  def icon_link_tag(source='/assets/icon.png', options={})
    tag('link', {
        :rel => 'shortcut icon',
        :type => 'image/x-icon',
        :href => path_to_image(source)
    }.merge(options.symbolize_keys))
  end

  def apple_icon_link_tag(source='/assets/icon.png', options={})
    tag('link', {
        :rel => 'apple-touch-icon',
        :type => 'image/x-icon',
        :href => path_to_image(source)
    }.merge(options.symbolize_keys))
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  def yield_or_default(section, default = "")
    content_for?(section) ? content_for(section) : default
  end

  def tag_cloud(tags, classes)
    max = tags.sort_by(&:count).last
    tags.each do |tag|
      index = tag.count.to_f / max.count * (classes.size - 1)
      yield(tag, classes[index.round])
    end
  end
end
