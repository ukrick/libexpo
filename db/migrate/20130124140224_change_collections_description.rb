class ChangeCollectionsDescription < ActiveRecord::Migration
  def self.up
    change_column :collections, :description, :text
  end

  def self.down
    change_column :collections, :description, :string
  end
end
