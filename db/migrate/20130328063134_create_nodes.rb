class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.boolean :is_public, :default => 0

      t.timestamps
    end
  end
end
