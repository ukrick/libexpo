class AddPublicFlags < ActiveRecord::Migration
  def change
    add_column :collections, :is_public, :boolean, :default => 0
    add_column :articles, :is_public, :boolean, :default => 0
  end
end
