class AddBookIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :book_id, :integer, after: :article_id
    add_index :comments, :book_id
  end
end
