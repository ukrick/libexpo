class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.references :user
      t.references :collection
      t.references :article
      t.references :book
      t.text :description
      t.boolean :is_public

      t.timestamps
    end
    add_index :notes, :user_id
    add_index :notes, :collection_id
    add_index :notes, :article_id
    add_index :notes, :book_id
  end
end
