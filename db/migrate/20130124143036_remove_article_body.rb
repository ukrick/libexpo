class RemoveArticleBody < ActiveRecord::Migration
  def self.up
    remove_column :articles, :body
  end

  def self.down
    add_column :articles, :body, :text
  end
end
