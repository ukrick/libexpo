class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.references :user
      t.references :collection
      t.string :image
      t.text :description

      t.timestamps
    end
    add_index :books, :user_id
    add_index :books, :collection_id
  end
end
