class AddPublicFlagToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :is_public, :boolean, :default => 0
  end
end
