class AddAuthorIdToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :author_id, :integer, after: :user_id
    add_index :notes, :author_id
  end
end
