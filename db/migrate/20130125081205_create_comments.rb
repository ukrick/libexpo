class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user
      t.references :collection
      t.references :article
      t.text :description
      t.boolean :is_public, :default => 0

      t.timestamps
    end
    add_index :comments, :user_id
    add_index :comments, :collection_id
    add_index :comments, :article_id
  end
end
