class AddTitleToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :title, :string, after: :book_id

  end
end
