class AddCategoryIdToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :category_id, :integer, after: :author_id
    add_index :publications, :category_id
  end
end
