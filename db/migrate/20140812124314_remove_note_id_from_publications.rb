class RemoveNoteIdFromPublications < ActiveRecord::Migration
  def self.up
    remove_column :publications, :note_id
  end

  def self.down
    add_column :publications, :note_id, :integer
  end
end
