class AddNodeIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :node_id, :integer, :after => :user_id

  end
end
