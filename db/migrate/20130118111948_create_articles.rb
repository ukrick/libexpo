class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.references :user
      t.references :collection
      t.string :title
      t.string :description
      t.text :body

      t.timestamps
    end
    add_index :articles, :user_id
    add_index :articles, :collection_id
  end
end
