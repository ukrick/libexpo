class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.references :user
      t.references :category
      t.string :title
      t.string :description

      t.timestamps
    end
    add_index :collections, :user_id
    add_index :collections, :category_id
  end
end
