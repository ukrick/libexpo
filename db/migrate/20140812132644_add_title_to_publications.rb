class AddTitleToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :title, :string, after: :user_id

  end
end
