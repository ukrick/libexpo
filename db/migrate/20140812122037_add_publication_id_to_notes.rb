class AddPublicationIdToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :publication_id, :integer, after: :user_id
    add_index :notes, :publication_id
  end
end
