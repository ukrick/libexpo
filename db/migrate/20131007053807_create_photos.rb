class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.references :user
      t.references :collection
      t.string :image
      t.string :title
      t.text :description

      t.timestamps
    end
    add_index :photos, :user_id
    add_index :photos, :collection_id
  end
end
