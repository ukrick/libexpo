class AddProfileToUsers < ActiveRecord::Migration
  def change
    add_column :users, :image, :string
    add_column :users, :first_name, :string, :limit => 47
    add_column :users, :middle_name, :string, :limit => 47
    add_column :users, :last_name, :string, :limit => 47
    add_column :users, :mobile, :string, :limit => 23
    add_column :users, :title, :string
  end
end
