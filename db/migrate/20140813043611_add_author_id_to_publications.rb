class AddAuthorIdToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :author_id, :integer, after: :user_id
    add_index :publications, :author_id
  end
end
