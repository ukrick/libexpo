class AddPublicFlagToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_public, :boolean, :default => 0
  end
end
