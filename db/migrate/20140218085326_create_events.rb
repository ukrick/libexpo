class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :user
      t.string :title
      t.text :description
      t.string :image
      t.datetime :start
      t.datetime :end
      t.boolean :all_day
      t.string :slug

      t.timestamps
    end
    add_index :events, :user_id
    add_index :events, :slug
  end
end
