class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.references :user
      t.references :note
      t.text :description
      t.boolean :is_public
      t.string :slug

      t.timestamps
    end
    add_index :publications, :user_id
    add_index :publications, :note_id
  end
end
