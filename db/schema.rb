# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140819105600) do

  create_table "articles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.integer  "collection_id"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "image"
    t.string   "slug"
    t.boolean  "is_public",     :default => false
  end

  add_index "articles", ["collection_id"], :name => "index_articles_on_collection_id"
  add_index "articles", ["slug"], :name => "index_articles_on_slug"
  add_index "articles", ["user_id"], :name => "index_articles_on_user_id"

  create_table "books", :force => true do |t|
    t.integer  "user_id"
    t.integer  "collection_id"
    t.string   "image"
    t.text     "description"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "title"
    t.string   "slug"
  end

  add_index "books", ["collection_id"], :name => "index_books_on_collection_id"
  add_index "books", ["slug"], :name => "index_books_on_slug"
  add_index "books", ["user_id"], :name => "index_books_on_user_id"

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "slug"
    t.boolean  "is_public",   :default => false
  end

  add_index "categories", ["slug"], :name => "index_categories_on_slug"

  create_table "collections", :force => true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.text     "afterword"
    t.string   "slug"
    t.boolean  "is_public",   :default => false
  end

  add_index "collections", ["category_id"], :name => "index_collections_on_category_id"
  add_index "collections", ["slug"], :name => "index_collections_on_slug"
  add_index "collections", ["user_id"], :name => "index_collections_on_user_id"

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "collection_id"
    t.integer  "article_id"
    t.integer  "book_id"
    t.text     "description"
    t.boolean  "is_public",     :default => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "username"
  end

  add_index "comments", ["article_id"], :name => "index_comments_on_article_id"
  add_index "comments", ["book_id"], :name => "index_comments_on_book_id"
  add_index "comments", ["collection_id"], :name => "index_comments_on_collection_id"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "events", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.datetime "start"
    t.datetime "end"
    t.boolean  "all_day"
    t.string   "slug"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "is_public",   :default => false
  end

  add_index "events", ["slug"], :name => "index_events_on_slug"
  add_index "events", ["user_id"], :name => "index_events_on_user_id"

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "nodes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "slug"
    t.boolean  "is_public",   :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "notes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "author_id"
    t.integer  "publication_id"
    t.integer  "collection_id"
    t.integer  "article_id"
    t.integer  "book_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "is_public"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "notes", ["article_id"], :name => "index_notes_on_article_id"
  add_index "notes", ["author_id"], :name => "index_notes_on_author_id"
  add_index "notes", ["book_id"], :name => "index_notes_on_book_id"
  add_index "notes", ["collection_id"], :name => "index_notes_on_collection_id"
  add_index "notes", ["publication_id"], :name => "index_notes_on_publication_id"
  add_index "notes", ["user_id"], :name => "index_notes_on_user_id"

  create_table "photos", :force => true do |t|
    t.integer  "user_id"
    t.integer  "collection_id"
    t.string   "image"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "photos", ["collection_id"], :name => "index_photos_on_collection_id"
  add_index "photos", ["user_id"], :name => "index_photos_on_user_id"

  create_table "publications", :force => true do |t|
    t.integer  "user_id"
    t.integer  "author_id"
    t.integer  "category_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "is_public"
    t.string   "slug"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "image"
  end

  add_index "publications", ["author_id"], :name => "index_publications_on_author_id"
  add_index "publications", ["category_id"], :name => "index_publications_on_category_id"
  add_index "publications", ["user_id"], :name => "index_publications_on_user_id"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "rich_rich_files", :force => true do |t|
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "rich_file_file_name"
    t.string   "rich_file_content_type"
    t.integer  "rich_file_file_size"
    t.datetime "rich_file_updated_at"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.text     "uri_cache"
    t.string   "simplified_type",        :default => "file"
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "username"
    t.string   "role",                   :limit => 30
    t.string   "image"
    t.string   "first_name",             :limit => 47
    t.string   "middle_name",            :limit => 47
    t.string   "last_name",              :limit => 47
    t.string   "mobile",                 :limit => 23
    t.string   "title"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
