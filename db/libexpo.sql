CREATE DATABASE libexpo_development;
CREATE DATABASE libexpo_test;
CREATE DATABASE libexpo_production;
CREATE USER 'libexpo'@'127.0.0.1' IDENTIFIED BY 'libexpo';
GRANT ALL PRIVILEGES ON libexpo_development.* TO 'libexpo'@'127.0.0.1' IDENTIFIED BY 'libexpo' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON libexpo_test.* TO 'libexpo'@'127.0.0.1' IDENTIFIED BY 'libexpo' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON libexpo_production.* TO 'libexpo'@'127.0.0.1' IDENTIFIED BY 'libexpo' WITH GRANT OPTION;
FLUSH PRIVILEGES;