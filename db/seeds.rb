# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(
    email: 'anonymous@adidonntu.org.ua',
    password: 'anonymous',
    password_confirmation: 'anonymous',
    username: 'anonymous',
    role: 'Гость'
)
User.create!(
    email: 'kirillov.andy@gmail.com',
    password: 'administrator',
    password_confirmation: 'administrator',
    username: 'admin',
    role: 'Администратор'
)