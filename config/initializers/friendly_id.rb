require "babosa"

FriendlyId.defaults do |config|
  config.use [:slugged, :history]
  config.use Module.new {
    def normalize_friendly_id(text)
      text.to_slug.normalize.to_s
    end
  }
end